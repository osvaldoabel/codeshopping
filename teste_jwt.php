<?php 

// header - payload - signature
// [HEADER]
$header = [
    'alg' => 'HS256',
    'typ' => 'JWT'
];

$header_json = json_encode($header);
$header_base64 = base64_encode($header_json);

echo "Cabeçalho JSON :"  . $header_json. "\n";
echo "Cabeçalho base64: ". $header_base64. "\n";

// [PAYLOAD]
$payload = [
    'name' => 'Osvaldo Abel',
    'email' => 'osvaldo.abel@teste.com',
];

$payload_json = json_encode($payload);
$payload_base64 = base64_encode($payload_json);
echo "PAYLOAD JSON :"  . $payload_json. "\n";
echo "PAYLOAD base64: ". $payload_base64. "\n";


$key = 'kakjgbkasgajsgbksbgkjs5@gasjhgvbjshbg';

$signature = hash_hmac('sha256', "$header_base64.$payload_base64", $key, true);
$signature_base64 = base64_encode($signature);

echo "\n\n";
echo "Signature JWT: $signature_base64 \n";
$token = "$header_base64.$payload_base64.$signature_base64";
echo "TOKEN: $token \n";