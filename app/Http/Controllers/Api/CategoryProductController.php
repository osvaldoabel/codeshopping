<?php

namespace CodeShopping\Http\Controllers\Api;

use Illuminate\Http\Request;
use CodeShopping\Http\Controllers\Controller;
use CodeShopping\Http\Requests\CategoryProductRequest;
use CodeShopping\Http\Resources\CategoryProductResource;
use CodeShopping\Models\Product;
use CodeShopping\Models\Category;

class CategoryProductController extends Controller
{
    public function index(Category $category)
    {
        return new CategoryProductResource($category);
    }

    public function store(CategoryProductRequest $request, Category $category)
    {
        $changed = $category->products()->sync($request->products);
        $productsAttachedId = $changed['attached'];
        $products = Product::whereIn('id', $productsAttachedId)->get();
        
        $resources = new CategoryProductResource($category);

        return $products->count()
                ? response()->json($resources, 201)
                : [];
    }

    public function destroy($id)
    {
        //
    }
}
