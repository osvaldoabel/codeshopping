<?php

namespace CodeShopping\Http\Controllers\Auth;

use CodeShopping\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use CodeShopping\Models\User;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
