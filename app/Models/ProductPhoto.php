<?php

declare(strict_types=1);

namespace CodeShopping\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class ProductPhoto extends Model
{
    const BASE_PATH = 'app/public';
    const DIR_PRODUCTS = 'products';
    
    const PRODUCTS_PATH = self::BASE_PATH.'/'. self::DIR_PRODUCTS;
    
    protected $fillable = ['file_name', 'product_id'];
    
    public static function photosPath($productId)
    {
        $path = self::PRODUCTS_PATH;
        return storage_path("{$path}/{$productId}");
    }

    public static function createWithPhotosFiles(int $productId, array $files) : Collection
    {
        try{
            self::uploadFiles($productId, $files);
            \DB::beginTransaction();
            $photos = self::createPhotosModels($productId, $files);
            \DB::commit();
            return new Collection($photos);
        }catch(\Exception $e) {
            self::deleteFiles($productId, $files);
            \DB::rollback();
            throw $e;
        }
    }

    public function deleteWithPhoto() : bool
    {
        try{
            \DB::beginTransaction();
            $this->deletePhoto($this->file_name);
            $result = $this->delete();
            \DB::commit();
            return $result;
        }
        catch(\Exception $e) {
            \DB::rollback();
            throw $e;
        }        
    }

    public function updateWithPhotosFiles(UploadedFile $file) : ProductPhoto
    {
        try{
            self::uploadFiles($this->product_id, [$file]);
            \DB::beginTransaction();
            $this->deletePhoto($this->file_name);
            $this->file_name = $file->hashName();
            $this->save();
            \DB::commit();
            return $this;
        }
        
        catch(\Exception $e) {
            self::deleteFiles($this->product_id, [$file]);
            \DB::rollback();
            throw $e;
        }
    }

    public function deletePhoto($filename)
    {
        $dir = self::photoDir($this->product_id);
        \Storage::disk('public')->delete("{$dir}/{$filename}");

    }
    
    private static function deleteFiles(int $productId, array $files)
    {
        foreach($files as $file) {
            $path       = self::photosPath($productId);
            $photoPath  = "{$path}/$file->hashName()";
            
            if(file_exists($photoPath)) {
                \File::delete($photoPath);
            }
        }
    }
    
    public static function uploadFiles($productId, array $files)
    {
        foreach($files as $file) {
            $dir = self::photoDir($productId);
            $file->store($dir, ['disk' => 'public']);
        }
    }

    private static function createPhotosModels(int $productId, array $files) : array
    {
        $photos = [];

        foreach($files as $file) {
            $photos[] = self::create([
                'file_name' => $file->hashName(),
                'product_id' => $productId
            ]);
        }

        return $photos;
    }
    
    public function getPhotoUrlAttribute()
    {
        $path = self::photoDir($this->product_id);
        return asset("storage/{$path}/{$this->file_name}");
    }
    
    public static function photoDir($productId)
    {
        $dir = self::DIR_PRODUCTS;
        return "{$dir}/{$productId}";
    }


    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }
}
